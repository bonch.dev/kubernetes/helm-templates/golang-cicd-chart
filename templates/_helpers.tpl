{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 24 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "fullname" -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- printf "%s-%s" .Release.Name $name | trimSuffix "-app" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "appname" -}}
{{- $releaseName := default .Release.Name .Values.releaseOverride -}}
{{- printf "%s" $releaseName | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "trackableappname" -}}
{{- $trackableName := printf "%s-%s" (include "appname" .) .Values.application.generic.track -}}
{{- $trackableName | trimSuffix "-stable" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Get a hostname from URL
*/}}
{{- define "hostname" -}}
{{- . | trimPrefix "http://" | trimPrefix "https://" | trimSuffix "/" | quote -}}
{{- end -}}


{{/*
Werf image
*/}}
{{- define "werfimage" -}}
{{- $tpl := first . }}
{{- $project := last . }}
{{- $imageMap := $tpl.Values.werf.image }}
{{- get $imageMap $project | default $imageMap.app }}
{{- end -}}

{{/*
Get replicaCount of runner deployment
*/}}
{{- define "runnerReplicaCount" -}}
	{{- if (not .Values.application.runner.kubernetes.leaderelections) -}}
	{{- 1 -}}
	{{- else -}}
	{{- default .Values.application.generic.replicaCount .Values.application.runner.replicaCount | int -}}
	{{- end -}}
{{- end -}}

{{/*
wait_for
*/}}
{{- define "wait_for" -}}
{{- if or .Values.postgresql.enabled (or .Values.rabbitmq.enabled .Values.keydb.enabled) -}}
initContainers:
  - name: wait-for
    image: registry.gitlab.com/bonch.dev/kubernetes/docker-images/wait-for:v2.2.1
    command: ['/wait-for']
    args:
{{- if .Values.postgresql.enabled -}}
{{- print "- --host=\"postgres:5432\"" | nindent 6 -}}
{{- end }}
{{- if .Values.rabbitmq.enabled }}
{{- print "- --host=\"rabbitmq:5672\"" | nindent 6 -}}
{{- end }}
{{- if .Values.keydb.enabled }}
{{- print "- --host=\"keydb:6379\"" | nindent 6 -}}
{{- end }}
{{- if .Values.redis.enabled }}
{{- print "- --host=\"redis-master:6379\"" | nindent 6 -}}
{{- end }}
{{- print "- --verbose" | nindent 6 -}}
{{- print "- --timeout=120s" | nindent 6 -}}
{{- end -}}
{{- end -}}

{{/*
Secret exists - cheks, than .Values.secret.env exists
*/}}
{{- define "isSecretExists" -}}
{{- if .Values.secret -}}
{{- if .Values.secret.env -}}
{{- printf "true" -}}
{{- end -}}
{{- else -}}
{{- printf "false" }}
{{- end -}}
{{- end -}}
